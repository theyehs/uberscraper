"""
Entry point of periodical scraping execution.
This script is expected to run by crond or manually, shutdown at any point, and start from where left off.
It's also possible for multiple instances of this scrpit running, possibly from different host too

We abstractly treat every scraping operation as a ScrapeTask, where a Task can be:
* a reading of a listing page (e.g. xxx category), to extract valid document link 
* scraping of a text document and reformat
* reading of html document to scrape binary resource (image/video/audio) and save


This script needs to
* ensure success of scraping
   * control the pacing of ScrapeTask -- not too frequent from a single account/ip to be ban
   * recognize network and server failure, and either skip the Task or suspend all Tasks in the same domain
* communicate with other running scraping process to not overlap
* resiliently allow the process to shutdown and restart at any time


"""

